const {
Kind,
useAbsSendTime,
useSdesMid,
RTCSessionDescription,
RTCIceCandidate,
} = require('werift');

const {
answerType,
codecsSupported,
debugConfig,
kOpusCodec,
offerType,
} = require('./webrtc_config.js');

const iceConfig = {
  sdpSemantics: "unified-plan",
  iceTransportPolicy: "all",
  bundlePolicy: "max-bundle",
  rtcpMuxPolicy: "require",
  codecs: codecsSupported,
  debug: debugConfig,
  headerExtensions: {
    video: [useSdesMid(), useAbsSendTime()],
    audio: [useSdesMid(), useAbsSendTime()],
  },
  iceUseIpv4: true,
  iceUseIpv6: true,
  iceServers: [
    {
      "urls": "stun:stun.relay.metered.ca:80",
    },
    {
      "urls": "turn:standard.relay.metered.ca:80",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turn:standard.relay.metered.ca:80?transport=tcp",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turn:standard.relay.metered.ca:443",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turns:standard.relay.metered.ca:443?transport=tcp",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    }
  ]
};
module.exports = iceConfig;
