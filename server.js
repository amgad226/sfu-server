
const {
  Kind,
  useAbsSendTime,
  useSdesMid,
  RTCSessionDescription,
  RTCIceCandidate,
} = require('werift');

const {
  answerType,
  codecsSupported,
  debugConfig,
  kOpusCodec,
  offerType,
} = require('./webrtc_config.js');
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const webrtc = require("werift");
const server = require("http").Server(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});
const  iceConfig  =require("./iceConfig.js");

const port = 8000;
let senderStreams = [];
let senderPeers = [];
let receivePeers = [];

// app.use(express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get("/amgad", (req, res) => res.send("hii aziz "));


app.get("/*", (req, res) => res.send("H Aziz"));

function handleTrackEvent(e, socketId) {
  const index = senderStreams.findIndex((item) => socketId === item.socketId);
  if (index != -1) {
    senderStreams[index].stream = e.streams[0];
  } else {
    senderStreams.push({
      socketId: socketId,
      stream: e.streams[0],
    });
  }
}

async function createPeerConnectionSend(sdp, socketId) {
  const peer = new webrtc.RTCPeerConnection(iceConfig);
  // // Set up header extensions for video and audio
  // const videoHeaderExtensions = [useSdesMid(), useAbsSendTime()];
  // const audioHeaderExtensions = [useSdesMid(), useAbsSendTime()];

  // // Add header extensions for video and audio
  // videoHeaderExtensions.forEach(extension => {
  //   peer.addTransceiver("video", { direction: "sendrecv", sendEncodings: [], headerExtensions: [extension] });
  // });
  // audioHeaderExtensions.forEach(extension => {
  //   peer.addTransceiver("audio", { direction: "sendrecv", sendEncodings: [], headerExtensions: [extension] });
  // });

  // Set up additional options
  // peer.iceUseIpv4 = true;
  // peer.iceUseIpv6 = true;
  // peer.codecs ={
  //   // audio: [kOpusCodec],
  //   video: "video/VP8",
  // },
  // peer.debug = debugConfig;
  console.log("enter send ");
  // const iceGatheringCompletePromise = new Promise((resolve, reject) => {
    peer.onicecandidate = (event) => {
      if (event.candidate) {
        // if (event.candidate.relatedAddress !== 'null') {
          // console.log("Relay candidate with valid relatedAddress found:");
          io.to(socketId).emit("SENDER-CANDIDATE",{
            candidate: event.candidate.candidate,
            sdpMid:event.candidate.sdpMid,
            sdpMLineIndex: event.candidate.sdpMLineIndex,
            socketId:socketId
          });
        // }
      } else {
        console.log("ICE Candidate gathering complete.");
      }
    };
    
    peer.onconnectionstatechange = () => {
      console.log("Peer connection state for sending with socket ID:", socketId, peer.connectionState);
      if (peer.connectionState === "connected") {
        console.log("Peer connection established successfully for sending with socket ID:", socketId);
      }
    };
    const peerObj = {
      socketId: socketId,
      peer: peer
    };
    senderPeers.push(peerObj);
    peer.ontrack = (e) => handleTrackEvent(e, socketId);
    // console.log("sdp is : ",sdp)
    const sdpDesc = {
      type: "offer",
      sdp: sdp,
    };
    const desc = new webrtc.RTCSessionDescription(sdp,'offer');
    // console.log("desc is ",desc);
    await peer.setRemoteDescription(desc);
    const answer = await peer.createAnswer();
    await peer.setLocalDescription(answer);
    const payload = peer.localDescription.sdp;
    console.log(" after enter send ");
  return payload;
}

async function createPeerConnectionReceive(sdp, socketId,mySocketId) {
  const peer = new webrtc.RTCPeerConnection(iceConfig);
  // // Set up header extensions for video and audio
  // const videoHeaderExtensions = [useSdesMid(), useAbsSendTime()];
  // const audioHeaderExtensions = [useSdesMid(), useAbsSendTime()];

  // // Add header extensions for video and audio
  // videoHeaderExtensions.forEach(extension => {
  //   peer.addTransceiver("video", { direction: "sendrecv", sendEncodings: [], headerExtensions: [extension] });
  // });
  // audioHeaderExtensions.forEach(extension => {
  //   peer.addTransceiver("audio", { direction: "sendrecv", sendEncodings: [], headerExtensions: [extension] });
  // });

  // Set up additional options
  // peer.iceUseIpv4 = true;
  // peer.iceUseIpv6 = true;
  // peer.codecs ={
  //   audio: [kOpusCodec],
  //   video: "video/H264",
  // };
  // peer.debug = debugConfig;


  const peerObj = {
    socketId: socketId,
    mySocketId: mySocketId,
    peer: peer
  };

  // Add the peer object to the list
  receivePeers.push(peerObj);
  // peer.onconnectionstatechange = () => {
  //   console.log("Receive Peer connection state for sending with socket ID:", socketId, peer.connectionState);
  //   if (peer.connectionState === "connected") {
  //     console.log( "Receive Peer connection established successfully for sending with socket ID:", socketId);
  //   }
  // };
  const sdpDesc = {
    type: "offer",
    sdp: sdp,
  };
  const desc = new webrtc.RTCSessionDescription(sdp,'offer');
  await peer.setRemoteDescription(desc);
  const index = senderStreams.findIndex((e) => e.socketId === socketId);


  if (senderStreams.length > 0) {
    if(senderStreams[index] !=null){
    senderStreams[index].stream
      .getTracks()
      .forEach((track) => peer.addTrack(track, senderStreams[index].stream));}
  }
  // await iceGatheringCompletePromise;
  const answer = await peer.createAnswer();
  await peer.setLocalDescription(answer);
  const payload = peer.localDescription.sdp;
  peer.onicecandidate = (event) => {
    if (event.candidate) {
      // if (event.candidate.relatedAddress !== 'null') {
      io.to(mySocketId).emit("RECEIVE-CANDIDATE",{
        candidate: event.candidate.candidate,
        sdpMid: event.candidate.sdpMid,
        sdpMLineIndex: event.candidate.sdpMLineIndex,
        socketId: socketId
      });
    // }
    
    } else {
      // console.log("RECEIVER ICE Candidate gathering complete.");
    }
  };

  return payload;
}


async function editPeerSender(candidate,sdpMid,sdpMLineIndex,socketId) {
  // console.log("candidate before is : ",candidate);
  const peerObj = senderPeers.find(peer => peer.socketId === socketId);
  if (peerObj) {
  try {
    peerObj.peer.onconnectionstatechange = () => {
      console.log("edit sender  Peer connection state for sending with socket ID:", socketId, peerObj.peer.connectionState);
      if (peerObj.peer.connectionState === "connected") {
        // console.log( "edit sender Peer connection established successfully for sending with socket ID:", socketId);
      }
    };
    const iceCandidate = new webrtc.RTCIceCandidate({
      candidate: candidate,
      sdpMid: sdpMid,
      sdpMLineIndex : sdpMLineIndex
    });
    await peerObj.peer.addIceCandidate(iceCandidate)
    .then(() => {
      // console.log("edit sender ICE candidate added successfully");
    })

    .catch((error) => {
      console.error("edit sender Error adding ICE candidate:", error);
    });
    } catch (error) {
      console.error("edit sender Error adding ICE candidate to peer:", error);
    }
  } else {
    console.error("edit sender Peer with socket ID", socketId, "not found.");
  }
}

async function editPeerReceiver(candidate,sdpMid,sdpMLineIndex,mySocketId, socketId) {
  console.log("_______receive peer from :",mySocketId , "to peer : ",socketId);
  const peerObj = receivePeers.find(peer => peer.socketId === socketId && peer.mySocketId === mySocketId);
  if (peerObj) {
    try {
      const iceCandidate = new webrtc.RTCIceCandidate({
        candidate: candidate,
        sdpMid: sdpMid,
        sdpMLineIndex : sdpMLineIndex
      });
      await peerObj.peer.addIceCandidate(iceCandidate)
      .then(() => {
        // console.log("edit receiver ICE candidate added successfully");
      })
      .catch((error) => {
        console.error("Error adding ICE candidate:", error);
      });
      } catch (error) {
        console.error("Error adding ICE candidate to peer:", error);
      }
    } else {
      console.error("Peer with socket ID", socketId, "not found.");
    }

}

io.on("connection", function (socket) {
  socket.on("SEND-CSS", async function (data) {
    // console.log("SEND-CSS")
    const payload = await createPeerConnectionSend(data.sdp, socket.id);
    const listSocketId = senderStreams
      .filter((e) => e.socketId != socket.id)
      .map((e) => e.socketId);
      console.log("the user:",socket.id,"receive this list: \n ",listSocketId);

    io.to(socket.id).emit("SEND-SSC", {
      socketId: socket.id,
      sdp: payload,
      sockets: listSocketId,
    });
    socket.broadcast.emit("NEW-PEER-SSC", {
      socketId: socket.id,
    });
  });

  socket.on("RECEIVE-CSS", async function (data) {
    const payload = await createPeerConnectionReceive(data.sdp, data.socketId,socket.id);
    io.to(socket.id).emit("RECEIVE-SSC", {
      socketId: data.socketId,
      sdp: payload,
    });
  });
  socket.on("ICE-CANDIDATE",async function(data){
    io.to(data.userToSignal).emit("ICE-CANDIDATE",{
      candidate: data.candidate,
      socketId : socket.id,
    })
  });
  socket.on("ICE-CANDIDATE-SENDER",async function(data){
    // console.log("data:",data); 
    await editPeerSender(data.candidate,data.sdpMid,data.sdpMLineIndex,data.socketId);
  })
  socket.on("ICE-CANDIDATE-RECEIVER",async function(data){
    await editPeerReceiver(data.candidate,data.sdpMid,data.sdpMLineIndex,socket.id,data.socketId);
  })
  // socket.on("ICE-CANDIDATE-RECEIVER",async function(data){
  //   await editPeerReceiver(data.candidate, data.socketId);
  // });
  // socket.on("ICE-CANDIDATE-BROADCAST",async function(data){
  //   socket.broadcast.emit("ICE-CANDIDATE",{
  //     socketId  :   socket.id,
  //     candidate : data.candidate
  //   })
  // });

  socket.on("kick-all",function (){
    senderStreams = [];
    senderPeers = [];
    receivePeers = [];
  })
  socket.on("disconnect", function () {
    // console.log("disconnect")
    console.log("User disconnected:", socket.id);
    io.emit("user-disconnected", { userId: socket.id });

    senderStreams = senderStreams.filter((e) => e.socketId !== socket.id);
  });
});

app.post("/broadcast", async ({ body }, res) => {
  const peer = new webrtc.RTCPeerConnection(iceConfig);
  peer.ontrack = (e) =>
    handleTrackEvent(e, Math.floor(Math.random() * 1000000000).toString());
  const desc = new webrtc.RTCSessionDescription(body.sdp);
  await peer.setRemoteDescription(desc);
  const answer = await peer.createAnswer();
  await peer.setLocalDescription(answer);
  const payload = {
    sdp: peer.localDescription,
  };

  res.json(payload);
});

app.post("/consumer", async ({ body }, res) => {
  const peer = new webrtc.RTCPeerConnection(iceConfig);
  const desc = new webrtc.RTCSessionDescription(body.sdp);
  await peer.setRemoteDescription(desc);
  if (senderStreams.length > 0) {
    let index = 0;
    senderStreams[index].stream
      .getTracks()
      .forEach((track) => peer.addTrack(track, senderStreams[index].stream));
  }
  const answer = await peer.createAnswer();
  await peer.setLocalDescription(answer);
  const payload = {
    sdp: peer.localDescription,
  };

  res.json(payload);
});

server.listen(port, function () {
  console.log("Server is running on port: " + port);
});
